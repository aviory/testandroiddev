package com.avior.cgs.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.avior.cgs.R;
import com.avior.cgs.datastorage.model.PictureModel;
import com.avior.cgs.utils.Constants;
import java.util.HashMap;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by avior on 23.09.2015.
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private final static String TAG = SearchAdapter.class.getSimpleName();
    private Context context;
    private List<PictureModel> pictureModels;
    private MyAdapterEventListener myAdapterEventListener;
    private HashMap<ViewHolder,ProgressBar> progressBarHashMap;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public MyAdapterEventListener myAdapterEventListener;
        public List<PictureModel> pictureModels;
        @Bind(R.id.tvNumber)
        protected TextView tvNumber;
        @Bind(R.id.tvUrl)
        public TextView tvUrl;
        @Bind(R.id.imgView)
        public ImageView imgView;
        @Bind(R.id.progressBar)
        protected ProgressBar progressBar;
        @Bind(R.id.checkbox)
        public CheckBox checkbox;
        public String url;
        public int position;
        public ViewHolder(RelativeLayout v) {
            super(v);
            ButterKnife.bind(this, v);
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d(TAG, "CheckBox: " + isChecked);
                    pictureModels.get(position).setCheck(isChecked);
                    myAdapterEventListener.onCheckoBoxChange(isChecked, position);
                }
            });
            imgView.setOnClickListener(v1 -> myAdapterEventListener.onClickImage(url));
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SearchAdapter(Context context, List<PictureModel> pictureModels) {
        this.context = context;
        this.pictureModels = pictureModels;
        progressBarHashMap = new HashMap<>();
    }

    public void setMyAdapterEventListener(MyAdapterEventListener myAdapterEventListener) {
        this.myAdapterEventListener = myAdapterEventListener;
    }

    public List<PictureModel> getPictureModels() {
        return pictureModels;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_item_search, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvNumber.setText(""+(position+1));
        holder.myAdapterEventListener = myAdapterEventListener;
        holder.pictureModels = pictureModels;
        PictureModel pictureModel = pictureModels.get(position);
        holder.position = position;
        holder.url = pictureModel.getUrl();
        holder.progressBar.setVisibility(View.VISIBLE);
        progressBarHashMap.put(holder, holder.progressBar);
        Picasso.with(context)
                .load(holder.url)
                .resize(75, 50)
                .error(R.drawable.error)
                .into(holder.imgView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        progressBarHashMap.get(holder).setVisibility(View.GONE);
                        progressBarHashMap.remove(holder);
                    }

                    @Override
                    public void onError() {
                        progressBarHashMap.get(holder).setVisibility(View.GONE);
                        progressBarHashMap.remove(holder);
                    }
                });
        holder.tvUrl.setText(Html.fromHtml(pictureModel.getTitle()));
        if (pictureModel.isCheck()) holder.checkbox.setChecked(true);
        else holder.checkbox.setChecked(false);

        if (position==pictureModels.size()-1&&pictureModels.size()< Constants.MAX_INDEX) {
            myAdapterEventListener.onEndList(pictureModels.size());
        }
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return pictureModels.size();/*mDataset.length;*/
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "Position: " + position);
        return super.getItemId(position);
    }

    public interface MyAdapterEventListener {
        public void onClickImage(String url);
        public void onCheckoBoxChange(boolean check, int position);
        public void onEndList(int size);
    }


}