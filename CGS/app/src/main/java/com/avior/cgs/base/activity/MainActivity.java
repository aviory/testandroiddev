package com.avior.cgs.base.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.avior.cgs.R;
import com.avior.cgs.base.base.BaseActivity;
import com.avior.cgs.base.fragment.MemoryFragment;
import com.avior.cgs.base.fragment.SavedPicturesFragment;
import com.avior.cgs.base.fragment.SearchPictureFragment;

import butterknife.Bind;

public class MainActivity extends BaseActivity {
    private final static String TAG = MainActivity.class.getSimpleName();
    private PagerAdapter mPagerAdapter;
    private MemoryFragment memoryFragment;

    @Bind(R.id.pager)
    protected ViewPager mPager;
    @Bind(R.id.toolbar)
    protected Toolbar toolbar;

    @Override
    protected int setLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        initViewPager();
    }

    private void initViewPager() {
        initAdapter();
        mPager.setAdapter(mPagerAdapter);
        Log.d(TAG, "Count: "+mPagerAdapter.getCount());
    }

    private void initAdapter() {
        memoryFragment = (MemoryFragment) getSupportFragmentManager()
                .findFragmentByTag(MemoryFragment.class.getCanonicalName());
        if (memoryFragment==null) {
            Log.d(TAG, "Memory fragment is isn't.");
            memoryFragment = new MemoryFragment();

            mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
            getSupportFragmentManager().beginTransaction()
                    .add(memoryFragment, MemoryFragment.class.getCanonicalName())
                    .commit();
            memoryFragment.setmPagerAdapter(mPagerAdapter);
        } else {
            Log.d(TAG, "Memory fragment is it." );
            mPagerAdapter = memoryFragment.getmPagerAdapter();
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private SavedPicturesFragment savedPicturesFragment;
        private SearchPictureFragment searchPictureFragment;

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
            Log.d(TAG, "Create new fragments");
            searchPictureFragment = new SearchPictureFragment();
            savedPicturesFragment = new SavedPicturesFragment();
        }

        @Override
        public Fragment getItem(int position) {
            return position!=1 ? searchPictureFragment : savedPicturesFragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


}
