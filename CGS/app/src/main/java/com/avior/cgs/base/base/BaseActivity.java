package com.avior.cgs.base.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by avior on 22.09.2015.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @LayoutRes
    protected abstract int setLayoutRes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayoutRes());
        ButterKnife.bind(this);
    }
}
