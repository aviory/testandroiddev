package com.avior.cgs.adapters;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.avior.cgs.R;
import com.squareup.picasso.Picasso;
import com.avior.cgs.datastorage.cursor_loader.SavedPictureCursorLoader;
import com.avior.cgs.datastorage.model.PictureModel;


import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DBCursorAdapter extends RecyclerView.Adapter<DBCursorAdapter.ViewHolder> {
    private final static String TAG = DBCursorAdapter.class.getSimpleName();

    private Cursor dataCursor;
    private Context context;
    private DataSetObserver mDataSetObserver;
    private ChangeObserver mChangeObserver;
    private boolean mDataValid;
    private HashMap<ViewHolder,ProgressBar> progressBarHashMap;
    private OnClickItemListener onClickItemListener;
    private SavedPictureCursorLoader cursorLoader;

    public DBCursorAdapter(Context context, OnClickItemListener listener) {
        Log.d(TAG, "Create adapter!");
        this.context = context;
        onClickItemListener = listener;
        progressBarHashMap = new HashMap<>();
        cursorLoader = new SavedPictureCursorLoader(context);
        initDataChengeObservers();
        initCursor();
    }

    private void initDataChengeObservers() {
        mDataSetObserver = new NotifyingDataSetObserver();
        mChangeObserver = new ChangeObserver();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_item_saved_picture, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PictureModel model = getItem(position);
        progressBarHashMap.put(holder, holder.progressBar);
        holder.tvNumber.setText("" + position);
        holder.tvUrl.setText(Html.fromHtml(model.getTitle()));
        holder.imgView.setOnClickListener(v -> onClickItemListener.onClickImage(model.getUrl()));
        holder.tvUrl.setOnClickListener(v -> Log.d(TAG, "Title"));

        holder.progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context)
                .load(model.getUrl())
                .resize(75, 50)
                .error(R.drawable.error)
                .into(holder.imgView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        progressBarHashMap.get(holder).setVisibility(View.GONE);
                        progressBarHashMap.remove(holder);
                    }

                    @Override
                    public void onError() {
                        progressBarHashMap.get(holder).setVisibility(View.GONE);
                        progressBarHashMap.remove(holder);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return (dataCursor == null) ? 0 : dataCursor.getCount();
    }

    public void initCursor(){
        changeCursor(cursorLoader.loadInBackground());
        mDataValid = dataCursor != null;
    }

    public void changeCursor(Cursor cursor) {
        Cursor old = swapCursor(cursor);
        if (old != null) {
            old.unregisterContentObserver(mChangeObserver);
            old.unregisterDataSetObserver(mDataSetObserver);
            old.close();
        }
    }

    public Cursor swapCursor(Cursor cursor) {
        if (dataCursor == cursor) {
            return null;
        }
        Cursor oldCursor = dataCursor;
        this.dataCursor = cursor;
        if (cursor != null) {
            cursor.registerDataSetObserver(mDataSetObserver);
            cursor.registerContentObserver(mChangeObserver);
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }

    // Load data from dataCursor and return it...
    private PictureModel getItem(int position) {
        dataCursor.moveToPosition(position);
        PictureModel model = new PictureModel();
        model.setTitle(dataCursor.getString(1));
        model.setUrl(dataCursor.getString(2));
        return model;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvNumber)
        protected TextView tvNumber;
        @Bind(R.id.tvUrl)
        public TextView tvUrl;
        @Bind(R.id.imgView)
        public ImageView imgView;
        @Bind(R.id.progressBar)
        protected ProgressBar progressBar;
        public ViewHolder(RelativeLayout v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    protected void onContentChanged() {
        initCursor();
        notifyDataSetChanged();
    }

    private class ChangeObserver extends ContentObserver {
        public ChangeObserver() {
            super(new Handler());
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            onContentChanged();
        }
    }

    private class NotifyingDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            mDataValid = true;
            notifyDataSetChanged();
        }

        @Override
        public void onInvalidated() {
            super.onInvalidated();
            mDataValid = false;
            notifyDataSetChanged();
        }
    }

    public interface OnClickItemListener{
        public void onClickImage(String url);
        public void onClickItem(PictureModel model);
    }
}