package com.avior.cgs.network;

import android.content.Context;

import com.avior.cgs.R;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

/**
 * Created by avior on 27.09.2015.
 */
public class RetrofitErrorHandler implements ErrorHandler {
    private Context context;

    public RetrofitErrorHandler(Context context) {
        this.context = context;
    }

    @Override
    public Throwable handleError(RetrofitError cause) {

        if (cause.getKind().equals(RetrofitError.Kind.NETWORK)) {
            if(cause.getMessage().contains("authentication")){
                //401 errors
                return  new Exception(context.getString(R.string.error_invalid_credentials));
            }else if (cause.getCause() instanceof SocketTimeoutException) {
                //Socket Timeout
                return new SocketTimeoutException(context.getString(R.string.error_connection_timeout));
            } else {
                //No Connection
                return new ConnectException(context.getString(R.string.error_no_connection));
            }
        } else {

            return cause;
        }
    }

}