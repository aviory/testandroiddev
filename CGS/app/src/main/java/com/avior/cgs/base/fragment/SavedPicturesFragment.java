package com.avior.cgs.base.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.avior.cgs.R;
import com.avior.cgs.base.base.BaseFragment;
import com.avior.cgs.datastorage.cursor_loader.SavedPictureCursorLoader;
import com.avior.cgs.datastorage.model.PictureModel;
import com.avior.cgs.adapters.DBCursorAdapter;
import com.avior.cgs.utils.Constants;

import butterknife.Bind;

public class SavedPicturesFragment extends BaseFragment implements
        DBCursorAdapter.OnClickItemListener {
    private final static String TAG = SavedPicturesFragment.class.getSimpleName();

    @Bind(R.id.recyclerView)
    protected RecyclerView recyclerView;

    private DBCursorAdapter DBCursorAdapter;

    public final Uri ITEM_URI = Uri
            .parse("content://com.avior.cgs.datastorage.provider.SavedPicturesProvider/my_db");
    public final static int LOADER_ID = 12345;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_data;
    }

    @Override
    protected void afterCreateView() {
        setRetainInstance(true);
        initRecyclerView();
        SavedPictureCursorLoader savedPictureCursorLoader =
                new SavedPictureCursorLoader(getContext());
        DBCursorAdapter DBCursorAdapter = new DBCursorAdapter(getContext(), this);
        recyclerView.setAdapter(DBCursorAdapter);
    }


    private void initRecyclerView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onClickImage(String url) {
        PreviewImageFragment viewImageFragment = new PreviewImageFragment();
        Bundle args = new Bundle();
        args.putString(Constants.IMAGE_URL, url);
        viewImageFragment.setArguments(args);
        getFragmentManager().beginTransaction()
                .add(R.id.parentContainer, viewImageFragment,
                        PreviewImageFragment.class.getCanonicalName())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onClickItem(PictureModel model) {

    }
}
