package com.avior.cgs.base.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;
import com.avior.cgs.R;
import com.avior.cgs.base.base.BaseFragment;
import com.avior.cgs.utils.Constants;

import butterknife.Bind;
import butterknife.OnClick;

public class PreviewImageFragment extends BaseFragment {

    @Bind(R.id.imgView)
    protected ImageView imgView;
    @Bind(R.id.progressBar)
    protected ProgressBar progressBar;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_fullscreen_view;
    }

    @Override
    protected void afterCreateView() {
        setRetainInstance(true);
        Bundle args = getArguments();
        if (args!=null) {
            String url = args.getString(Constants.IMAGE_URL);
            if (!TextUtils.isEmpty(url)) {
                Picasso.with(getContext())
                        .load(url)
                        .error(R.drawable.error)
                        .into(imgView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }
                            @Override
                            public void onError() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            }
        }
    }

    @OnClick(R.id.imgView)
    protected void dummyClick(){}
}
