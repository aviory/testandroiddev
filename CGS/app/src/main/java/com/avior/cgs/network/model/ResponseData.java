package com.avior.cgs.network.model;

import java.util.List;

/**
 * Created by avior on 25.09.2015.
 */
public class ResponseData {
    private List<GoogleResult> results;

    public List<GoogleResult> getResults() {
        return results;
    }

    public void setResults(List<GoogleResult> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "results=" + results +
                '}';
    }
}
