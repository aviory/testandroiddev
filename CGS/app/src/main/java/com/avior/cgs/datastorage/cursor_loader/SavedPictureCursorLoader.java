package com.avior.cgs.datastorage.cursor_loader;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;

import com.avior.cgs.datastorage.provider.SavedPicturesProvider;

public class SavedPictureCursorLoader extends CursorLoader {
    private final static String TAG = SavedPictureCursorLoader.class.getSimpleName();

    //private final ForceLoadContentObserver mObserver = new ForceLoadContentObserver();

    public SavedPictureCursorLoader(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    public SavedPictureCursorLoader(Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {
        Cursor cursor = getContext().getContentResolver().query(SavedPicturesProvider.ITEM_CONTENT_URI, null, null,
                null, null); // get your cursor from wherever you like

        return cursor;
    }

}