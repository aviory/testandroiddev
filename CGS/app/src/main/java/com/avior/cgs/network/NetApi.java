package com.avior.cgs.network;

import com.avior.cgs.network.model.Container;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by avior on 25.09.2015.
 */
public interface NetApi {

    @GET("/images?v=1.0")
    public void getResult(
            @Query("start") String start,
            @Query("rsz") String size,
            @Query("q") String search,
            Callback<Container> curatorCallback);
}