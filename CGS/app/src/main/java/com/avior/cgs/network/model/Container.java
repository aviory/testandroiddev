package com.avior.cgs.network.model;

public class Container {

    private ResponseData responseData;

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "Container{" +
                "responseData=" + responseData +
                '}';
    }
}